const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

function printFullName(e) {
	spanFullName.innerHTML = txtFirstName.value+" "+txtLastName.value;
};

txtFirstName.addEventListener('keyup', printFullName);
txtLastName.addEventListener('keyup', printFullName);